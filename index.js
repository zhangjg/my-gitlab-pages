const globby = require('globby')
const path = require('path');
const {Observable} = require('rxjs');
const matter = require('gray-matter');
const fs = require('fs');
const pug = require('pug')
const mkdirp = require('mkdirp');
const org = require('org');
const marked = require('marked');
const options={pretty:true}
const fn  = pug.compileFile("./theme/post.jade",options);
const home=`${process.env.npm_package_config_home}`
const pageCount=process.env.npm_package_config_pageCount;
const fnIndex = pug.compileFile("./theme/index.jade",options);

const postDir =process.env.npm_package_config_postDir
const glob=globby(`.${path.sep+postDir+path.sep}**${path.sep}*.{md,markdown,org}`,{absolute:false})
const readFile  = Observable.bindNodeCallback(fs.readFile);
const writeFile = Observable.bindNodeCallback(fs.writeFile);
const mkdir     = Observable.bindNodeCallback(mkdirp);
marked.setOptions({
    renderer: new marked.Renderer(),
    gfm: true,
    tables: true,
    breaks: false,
    pedantic: true,
    sanitize: false,
    smartLists: true,
    smartypants: true
});
const renderMarkdown=(md)=>{
    return marked(md);
}
const renderOrg=(orgContent)=>{
    var orgParser = new org.Parser();
    var orgDocument = orgParser.parse(orgContent);
    var orgHTMLDocument = orgDocument.convert(org.ConverterHTML, {
        headerOffset: 1,
        exportFromLineNumber: true,
        suppressSubScriptHandling: true,
        suppressAutoLink: false
    });
    return `${orgHTMLDocument.tocHTML}\n${orgHTMLDocument.contentHTML}`;
}
const generatorHTML=(doc)=>{
    switch(doc.ext){
        case '.md':
        case '.markdown':
        doc.content = renderMarkdown(doc.content);
        break;
        case '.org':
        doc.content = renderOrg(doc.content);
        break;
    }
    let dirName=doc.dir.replace(postDir,"public");
    let fileName=[dirName,doc.name].join(path.sep)+".html";
    let pageTitle=doc.data.title || doc.name
    //console.log('pageTitle:',pageTitle,"content:",doc.content);
    let html=fn({content:doc.content,home,pageTitle});
    return mkdir(dirName).flatMap( ()=> writeFile(fileName,html) )
            .map( ()=> {
                console.log('step3:'+fileName)
                doc.content=null;
                doc.dir=doc.dir.replace(postDir,"public");
                return doc;
        });
}

const generatorIndex=(posts,i)=> {
    let  prevName, nextName;
    if(i == 0 ){
        prevName="";
    }else if( i == 1){
        prevName="./index.html";
    }else{
        prevName=`./index.${i}.html`;
    }
    if(i+1 < posts.total ){
        nextName=`./index.${i+2}.html`;
    }else{
        nextName="";
    }
    const html=fnIndex({posts:posts,home,prevName,nextName});
    const dirname=[".","public"];
    let filename=[...dirname,"index"].join(path.sep);
    if(i == 0){
        filename+='.html';
    }else{
        filename+=`.${i+1}.html`;
    }
    return mkdir(dirname.join(path.sep) )
    .flatMap( ()=> writeFile(filename, html) )
    .map(()=>'setp 4:'+filename);
}

const server= (root)=>{
    const startServer= !!(process.argv[2]-0);
    const serverPort= process.env.npm_package_config_serverPort-0
    if(!startServer) {
        return 0;
    }else{
        console.log(`server start, listen on ${serverPort}`)
    }
    const http = require('http');
    http.createServer((req, res) => {
        console.log(req.url);
        let stream;
        switch(req.url){
            case "/":
            stream=fs.createReadStream([__dirname,"public",""].join(path.sep)+"index.html")
            break;
            default:
            stream=fs.createReadStream([__dirname,"public"].join(path.sep)+req.url);
            break;
        }
        stream.pipe(res);
    }).listen(serverPort);
}

Observable
    .from(glob)
    .flatMap(  xs=> xs )
    .map(
        file => {
            let ret = path.parse(file)
            ret.file=file;
            console.log("step1:",ret.file)
            return ret;
        }
    )
    .flatMap(
        doc=>
        readFile(doc.file)
            .map( x=>{
                let {data,content}=matter(x.toString());
                doc.data=data;
                doc.content=content;
                console.log('step2:',doc.file);
                return doc;
            })
    ).flatMap( generatorHTML )
    .reduce((ret,doc)=>{
            ret.push(doc);
            ret=ret.sort((a,b)=>a.file.localeCompare(b.file)).reverse();
            return ret
        },
        []
    ).flatMap(arr=>{
        return arr.reduce((ret,doc,i)=>{
            let g;
            if(i%pageCount == 0){
                g=[];
                g.total = Math.ceil(arr.length/pageCount);
                ret.push(g);
            }else{
                g=ret[ret.length-1];
            }
            g.push(doc);
            return ret;
        },[]);
    })
    .flatMap( generatorIndex )
    .subscribe(()=>{},console.error,()=>server("./public"));
